package sampleTest;

import javatestlibrary.TestCase;

public class MoneyTest extends TestCase {
    public static void main(String args[]) {
        TestCase t = new MoneyTest();
        t.run(MoneyTest.class);
    }

    public void testAssertTrue() {
        assertTrue(false);
    }

    public void testAssertFalse() {
        assertFalse(false);
    }

    public void testBagMultiply() {
        // {[12 CHF][7 USD]} *2 == {[24 CHF][14 USD]}
        Money f12CHF = new Money(12, "CHF");
        Money f7USD = new Money(7, "USD");
        IMoney fMB1 = MoneyBag.create(f12CHF, f7USD);
        IMoney expected = MoneyBag.create(new Money(24, "CHF"), new Money(14, "USD"));
        assertEqual(expected, fMB1.multiply(2));
        assertEqual(fMB1, fMB1.multiply(1));
        assertTrue(fMB1.multiply(0).isZero());
    }

    public void testBagNegate() {
        // {[12 CHF][7 USD]} negate == {[-12 CHF][-7 USD]}
        Money f12CHF = new Money(12, "CHF");
        Money f7USD = new Money(7, "USD");
        IMoney fMB1 = MoneyBag.create(f12CHF, f7USD);

        IMoney expected = MoneyBag.create(new Money(-12, "CHF"), new Money(-7, "USD"));
        assertEqual(expected, fMB1.negate());
    }

    public void testBagSimpleAdd() {
        // {[12 CHF][7 USD]} + [14 CHF] == {[26 CHF][7 USD]}
        Money f12CHF = new Money(12, "CHF");
        Money f7USD = new Money(7, "USD");
        Money f14CHF = new Money(14, "CHF");
        IMoney fMB1 = MoneyBag.create(f12CHF, f7USD);

        IMoney expected = MoneyBag.create(new Money(26, "CHF"), new Money(7, "USD"));
        assertEqual(expected, fMB1.add(f14CHF));
    }

    public void testBagSubtract() {

        // {[12 CHF][7 USD]} - {[14 CHF][21 USD] == {[-2 CHF][-14 USD]}
        Money f12CHF = new Money(12, "CHF");
        Money f7USD = new Money(7, "USD");
        Money f14CHF = new Money(14, "CHF");
        Money f21USD = new Money(21, "USD");
        IMoney fMB1 = MoneyBag.create(f12CHF, f7USD);
        IMoney fMB2 = MoneyBag.create(f14CHF, f21USD);

        IMoney expected = MoneyBag.create(new Money(-2, "CHF"), new Money(-14, "USD"));
        assertEqual(expected, fMB1.subtract(fMB2));
    }


    public void testBagSumAdd() {
        // {[12 CHF][7 USD]} + {[14 CHF][21 USD]} == {[26 CHF][28 USD]}
        Money f12CHF = new Money(12, "CHF");
        Money f7USD = new Money(7, "USD");
        Money f14CHF = new Money(14, "CHF");
        Money f21USD = new Money(21, "USD");
        IMoney fMB1 = MoneyBag.create(f12CHF, f7USD);
        IMoney fMB2 = MoneyBag.create(f14CHF, f21USD);

        IMoney expected = MoneyBag.create(new Money(26, "CHF"), new Money(28, "USD"));
        assertEqual(expected, fMB1.add(fMB2));
    }

    public void testIsZero() {
        Money f12CHF = new Money(12, "CHF");
        Money f7USD = new Money(7, "USD");
        Money f14CHF = new Money(14, "CHF");
        Money f21USD = new Money(21, "USD");
        IMoney fMB1 = MoneyBag.create(f12CHF, f7USD);
        IMoney fMB2 = MoneyBag.create(f14CHF, f21USD);

        assertTrue(fMB1.subtract(fMB1).isZero());
        assertTrue(MoneyBag.create(new Money(0, "CHF"), new Money(0, "USD")).isZero());
    }

    public void testMixedSimpleAdd() {
        // [12 CHF] + [7 USD] == {[12 CHF][7 USD]}
        Money f12CHF = new Money(12, "CHF");
        Money f7USD = new Money(7, "USD");

        IMoney expected = MoneyBag.create(f12CHF, f7USD);
        assertEqual(expected, f12CHF.add(f7USD));
    }

    public void testBagNotEquals() {
        Money f12CHF = new Money(12, "CHF");
        Money f7USD = new Money(7, "USD");

        IMoney bag = MoneyBag.create(f12CHF, f7USD);
        assertFalse(bag.equals(new Money(12, "DEM").add(f7USD)));
    }

    public void testMoneyBagEquals() {
        Money f12CHF = new Money(12, "CHF");
        Money f7USD = new Money(7, "USD");
        Money f14CHF = new Money(14, "CHF");
        Money f21USD = new Money(21, "USD");
        IMoney fMB1 = MoneyBag.create(f12CHF, f7USD);
        IMoney fMB2 = MoneyBag.create(f14CHF, f21USD);

        assertTrue(!fMB1.equals(null));

        assertEqual(fMB1, fMB1);
        IMoney equal = MoneyBag.create(new Money(12, "CHF"), new Money(7, "USD"));
        assertTrue(fMB1.equals(equal));
        assertTrue(!fMB1.equals(f12CHF));
        assertTrue(!f12CHF.equals(fMB1));
        assertTrue(!fMB1.equals(fMB2));
    }

    public void testMoneyBagHash() {
        Money f12CHF = new Money(12, "CHF");
        Money f7USD = new Money(7, "USD");
        IMoney fMB1 = MoneyBag.create(f12CHF, f7USD);

        IMoney equal = MoneyBag.create(new Money(12, "CHF"), new Money(7, "USD"));
        assertEqual(fMB1.hashCode(), equal.hashCode());
    }

    public void testMoneyEquals() {
        Money f12CHF = new Money(12, "CHF");
        Money f14CHF = new Money(14, "CHF");

        assertTrue(!f12CHF.equals(null));
        Money equalMoney = new Money(12, "CHF");
        assertEqual(f12CHF, f12CHF);
        assertEqual(f12CHF, equalMoney);
        assertEqual(f12CHF.hashCode(), equalMoney.hashCode());
        assertTrue(!f12CHF.equals(f14CHF));
    }

    public void testMoneyHash() {
        Money f12CHF = new Money(12, "CHF");

        assertTrue(!f12CHF.equals(null));
        Money equal = new Money(12, "CHF");
        assertEqual(f12CHF.hashCode(), equal.hashCode());
    }

    public void testSimplify() {
        IMoney money = MoneyBag.create(new Money(26, "CHF"), new Money(28, "CHF"));
        assertEqual(new Money(54, "CHF"), money);
    }

    public void testNormalize2() {
        // {[12 CHF][7 USD]} - [12 CHF] == [7 USD]
        Money f12CHF = new Money(12, "CHF");
        Money f7USD = new Money(7, "USD");
        IMoney fMB1 = MoneyBag.create(f12CHF, f7USD);

        Money expected = new Money(7, "USD");
        assertEqual(expected, fMB1.subtract(f12CHF));
    }

    public void testNormalize3() {
        // {[12 CHF][7 USD]} - {[12 CHF][3 USD]} == [4 USD]
        Money f12CHF = new Money(12, "CHF");
        Money f7USD = new Money(7, "USD");
        IMoney fMB1 = MoneyBag.create(f12CHF, f7USD);

        IMoney ms1 = MoneyBag.create(new Money(12, "CHF"), new Money(3, "USD"));
        Money expected = new Money(4, "USD");
        assertEqual(expected, fMB1.subtract(ms1));
    }

    public void testNormalize4() {
        // [12 CHF] - {[12 CHF][3 USD]} == [-3 USD]
        Money f12CHF = new Money(12, "CHF");

        IMoney ms1 = MoneyBag.create(new Money(12, "CHF"), new Money(3, "USD"));
        Money expected = new Money(-3, "USD");
        assertEqual(expected, f12CHF.subtract(ms1));
    }

    public void testPrint() {
        Money f12CHF = new Money(12, "CHF");

        assertEqual("[12 CHF]", f12CHF.toString());
    }

    public void testSimpleAdd() {
        // [12 CHF] + [14 CHF] == [26 CHF]
        Money f12CHF = new Money(12, "CHF");
        Money f14CHF = new Money(14, "CHF");

        Money expected = new Money(26, "CHF");
        assertEqual(expected, f12CHF.add(f14CHF));
    }

    public void testSimpleBagAdd() {
        // [14 CHF] + {[12 CHF][7 USD]} == {[26 CHF][7 USD]}
        Money f12CHF = new Money(12, "CHF");
        Money f7USD = new Money(7, "USD");
        Money f14CHF = new Money(14, "CHF");
        IMoney fMB1 = MoneyBag.create(f12CHF, f7USD);

        IMoney expected = MoneyBag.create(new Money(26, "CHF"), new Money(7, "USD"));
        assertEqual(expected, f14CHF.add(fMB1));
    }

    public void testSimpleMultiply() {
        // [14 CHF] *2 == [28 CHF]
        Money f14CHF = new Money(14, "CHF");

        Money expected = new Money(28, "CHF");
        assertEqual(expected, f14CHF.multiply(2));
    }

    public void testSimpleNegate() {
        // [14 CHF] negate == [-14 CHF]
        Money f14CHF = new Money(14, "CHF");

        Money expected = new Money(-14, "CHF");
        assertEqual(expected, f14CHF.negate());
    }

    public void testSimpleSubtract() {
        // [14 CHF] - [12 CHF] == [2 CHF]
        Money f12CHF = new Money(12, "CHF");
        Money f14CHF = new Money(14, "CHF");

        Money expected = new Money(2, "CHF");
        assertEqual(expected, f14CHF.subtract(f12CHF));
    }

    public void testObjectNotEqual() {
        // [14 CHF] - [12 CHF] == [2 CHF]
        Money f12CHF = new Money(12, "CHF");
        Money f14CHF = new Money(14, "CHF");

        Money expected = new Money(4, "CHF");
        notEqual(expected, f14CHF.subtract(f12CHF));
    }
}