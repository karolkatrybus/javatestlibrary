package sampleTest;

/**
 * The common interface for simple Monies and MoneyBags
 */
public interface IMoney {
    /**
     * Adds a sampleTest to this sampleTest.
     */
    IMoney add(IMoney m);

    /**
     * Adds a simple Money to this sampleTest. This is a helper method for
     * implementing double dispatch
     */
    IMoney addMoney(Money m);

    /**
     * Adds a MoneyBag to this sampleTest. This is a helper method for
     * implementing double dispatch
     */
    IMoney addMoneyBag(MoneyBag s);

    /**
     * Tests whether this sampleTest is zero
     */
    boolean isZero();

    /**
     * Multiplies a sampleTest by the given factor.
     */
    IMoney multiply(int factor);

    /**
     * Negates this sampleTest.
     */
    IMoney negate();

    /**
     * Subtracts a sampleTest from this sampleTest.
     */
    IMoney subtract(IMoney m);

    /**
     * Append this to a MoneyBag m.
     * appendTo() needs to be public because it is used
     * polymorphically, but it should not be used by clients
     * because it modifies the argument m.
     */
    void appendTo(MoneyBag m);
}
