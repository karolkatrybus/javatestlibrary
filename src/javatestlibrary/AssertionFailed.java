package javatestlibrary;

public class AssertionFailed extends Error {

    public AssertionFailed() {}

    public AssertionFailed(String message)
    {
        super(message);
    }
}
