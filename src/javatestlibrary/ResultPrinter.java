package javatestlibrary;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Method;
import java.util.ArrayList;

public class ResultPrinter extends JFrame {
    private ArrayList<Method> methods;
    private int runs;
    private long runTime;
    private int failed;
    private String message;

    public ResultPrinter(ArrayList<Method> methods, int runs, long runTime, int failed, String message, Class<?>
            testClass) {
        this.methods = methods;
        this.runs = runs;
        this.runTime = runTime;
        this.failed = failed;
        this.message = message;


        JFrame f = new JFrame("test");
        JPanel p = new JPanel(new GridLayout(4,1));
        JPanel p1 = new JPanel(new GridLayout(1,1));
        JPanel p2 = new JPanel(new GridLayout(1,2));
        JPanel p3 = new JPanel(new GridLayout(1,1));
        JPanel p4 = new JPanel(new GridLayout(1,8));

        JButton b = new JButton("Print Stack Trace");
        JComboBox combo = new JComboBox();
        JPanel resultPanel = new JPanel();
        JTextArea t = new JTextArea(message);
        JLabel emptyLabel = new JLabel("   ");
        JLabel testLabel = new JLabel("  Tests ");
        JLabel runsLabel = new JLabel("  Runs:");
        JLabel timeLabel = new JLabel("Time: ");
        JLabel failLabel = new JLabel("Failures: ");

        JLabel runsCountLabel = new JLabel();
        runsCountLabel.setText("" + runs);
        JLabel timeCountLabel = new JLabel();
        timeCountLabel.setText("" + runTime + " [ns]");
        JLabel failuresCountLabel = new JLabel();
        failuresCountLabel.setText("" + failed);


        Container contentpane = getContentPane();
        contentpane.setLayout(new GridLayout(2,3));

        for (Method m : methods) {
            combo.addItem(m);
        }
        combo.setRenderer(new MyObjectListCellRenderer());

        resultPanel.setSize(300, 20);
        resultPanel.setBackground(Color.GREEN);
        resultPanel.setVisible(true);
        if (failed > 0) resultPanel.setBackground(Color.RED);

        t.setLineWrap(true);

        p1.add(testLabel);
        p2.add(combo);
        p2.add(b);
        p3.add(resultPanel);
        p4.add(runsLabel);
        p4.add(runsCountLabel);
        p4.add(emptyLabel);
        p4.add(timeLabel);
        p4.add(timeCountLabel);
        p4.add(emptyLabel);
        p4.add(failLabel);
        p4.add(failuresCountLabel);

        p.add(p1);
        p.add(p2);
        p.add(p3);
        p.add(p4);

        JScrollPane scroll = new JScrollPane(t, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        contentpane.add(p);
        contentpane.add(scroll);

        f.setContentPane(contentpane);
        f.setBounds(100,100,650,400);
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String result = TestCase.runSelectedMethod((Method) combo.getSelectedItem(), testClass);
                if (result == "Success") {
                    resultPanel.setBackground(Color.GREEN);
                    failuresCountLabel.setText("0");
                    t.setText(result);
                } else {
                    resultPanel.setBackground(Color.RED);
                    failuresCountLabel.setText("1");
                    t.setText(result);
                }
                runsCountLabel.setText("1");
                timeCountLabel.setText("" + runTime + " [ns]");
            }
        });
    }

    /**
     * Inner class changing the displayed Strings of Combobox items
     */
    private class MyObjectListCellRenderer extends DefaultListCellRenderer {
        @Override
        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected,
                                                      boolean cellHasFocus) {
            if (value instanceof Method) {
                value = ((Method) value).getName();
            }
            super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            return this;
        }
    }
}
