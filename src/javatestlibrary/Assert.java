package javatestlibrary;

public abstract class Assert {

    protected Assert() {}

    public void assertTrue(boolean condition) {
        if (!condition) {
            throw new AssertionFailed();
        }
    }

    public void assertFalse(boolean condition) {
        if (condition) {
            throw new AssertionFailed();
        }
    }

    public void assertEqual(Object expected, Object actual) {
        if (expected == null && actual == null) {
            return;
        }
        if (expected != null && expected.equals(actual)) {
            return;
        }
        throw new AssertionFailed("");
    }

    public void assertEqual(int one, int two) {
        assertTrue(one==two);
    }

    public void assertEqual(double one, double two) {
        assertTrue(one==two);
    }

    public void assertEqual(float one, float two) {
        assertTrue(one==two);
    }

    public void notEqual(Object expected, Object actual) {
        assertFalse(expected.equals(actual));
    }

    public void notEqual(int one, int two) {
        assertFalse(one==two);
    }

    public void notEqual(double one, double two) {
        assertFalse(one==two);
    }

    public void notEqual(float one, float two) {
        assertFalse(one==two);
    }

    public void assertGreaterThan(int one, int two) {
        assertTrue(one > two);
    }

    public void assertGreaterThan(double one, double two) {
        assertTrue(one > two);    }

    public void assertGreaterThan(float one, float two) {
        assertTrue(one > two);
    }

    public void assertGreaterThan(int one, double two) {
        assertTrue((double) one > two);
    }

    public void assertGreaterThan(double one, int two) {
        assertTrue(one > (double) two);
    }

    public void assertGreaterThan(double one, float two) {
        assertTrue((float) one > two);
    }

    public void assertGreaterThan(float one, double two) {
        assertTrue(one > (float) two);
    }

    public void assertGreaterThan(int one, float two) {
        assertTrue((float) one > two);
    }

    public void assertGreaterThan(float one, int two) {
        assertTrue(one > (float) two);
    }

    public void assertGreaterOrEqual(int one, int two) {
        assertTrue(one >= two);
    }

    public void assertGreaterOrEqual(double one, double two) {
        assertTrue(one >=two);
    }

    public void assertGreaterOrEqual(float one, float two) {
        assertTrue(one >= two);
    }

    public void assertGreaterOrEqual(int one, double two) {
        assertTrue((double) one >= two);
    }

    public void assertGreaterOrEqual(double one, int two) {
        assertTrue(one >= (double) two);
    }

    public void assertGreaterOrEqual(double one, float two) {
        assertTrue((float) one >= two);
    }

    public void assertGreaterOrEqual(float one, double two) {
        assertTrue(one >= (float) two);
    }

    public void assertGreaterOrEqual(int one, float two) {
        assertTrue(one >= (double) two);
    }

    public void assertGreaterOrEqual(float one, int two) {
        assertTrue(one >= (float) two);
    }

    public void assertLessThan(int one, int two) {
        assertTrue(one < two);
    }

    public void assertLessThan(double one, double two) {
        assertTrue(one < two);
    }

    public void assertLessThan(float one, float two) {
        assertTrue(one < two);
    }

    public void assertLessThan(double one, float two) {
        assertTrue((float) one < two);
    }

    public void assertLessThan(float one, double two) {
        assertTrue(one < (float) two);
    }

    public void assertLessThan(int one, float two) {
        assertTrue((float) one < two);
    }

    public void assertLessThan(float one, int two) {
        assertTrue(one < (float) two);
    }

    public void assertLessThan(int one, double two) {
        assertTrue((double) one < two);
    }

    public void assertLessThan(double one, int two) {
        assertTrue(one < (double) two);
    }

    public void assertLessOrEqual(int one, int two) {
        assertFalse(one<=two);
    }

    public void assertLessOrEqual(double one, double two) {
        assertFalse(one<=two);
    }

    public void assertLessOrEqual(float one, float two) {
        assertFalse(one<=two);
    }

    public void assertLessOrEqual(double one, float two) {
        assertFalse((float) one<=two);
    }

    public void assertLessOrEqual(float one, double two) {
        assertFalse(one<= (float)two);
    }

    public void assertLessOrEqual(int one, float two) {
        assertFalse((float) one<=two);
    }

    public void assertLessOrEqual(float one, int two) {
        assertFalse(one <= (float) two);
    }

    public void assertLessOrEqual(int one, double two) {
        assertFalse((double) one <= two);
    }

    public void assertLessOrEqual(double one, int two) {
        assertFalse(one <= (double) two);
    }

    public void assertStringsEqual(String string1, String string2) {
        assertTrue(string1.equals(string2));
    }

    public void assertStringsNotEqual(String string1, String string2) {
        assertFalse(string1.equals(string2));
    }
}
