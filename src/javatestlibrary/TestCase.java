package javatestlibrary;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

public abstract class TestCase extends Assert {
    private ArrayList<Method> methods;
    private int succeded;
    private int failed;
    private String failedMessage = "Failed: \n";

    public TestCase() {
    }

    public static void main(String[] args) {

    }

    /**
     * Method used to invoke single method from class and print its stack trace
     *
     * @param m         - Method that is being invoked
     * @param testClass - Type of Class that tests operate in
     * @return Returns String with a message of either success or printed stack trace
     */
    public static String runSelectedMethod(Method m, Class<?> testClass) {
        String message = "Success";
        try {
            m.invoke(testClass.newInstance());
        } catch (InvocationTargetException e) {
            try {
                throw e.getCause();
            } catch (AssertionFailed a) {
                StringWriter sw = new StringWriter();
                PrintWriter pw = new PrintWriter(sw);
                a.printStackTrace(pw);
                message = sw.toString();
                return message;

            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
        return message;
    }

    /**
     * Runs all the tests, checks time, and opens window with results
     *
     * @param testClass - Type of Class that tests operate in
     */
    public void run(Class<? extends TestCase> testClass) {
        long startTime = System.currentTimeMillis();

        runTests(testClass);

        long endTime = System.currentTimeMillis();
        long runTime = endTime - startTime;
        String message = ((failed == 0) ? "Success" : failedMessage);
        int runs = succeded + failed;

        new ResultPrinter(methods, runs, runTime, failed, message, testClass);
    }

    /**
     * Invoke all methods ftom @param testClass
     *
     * @param testClass - Type of Class that tests operate in
     * @return Returns
     */
    public void runTests(Class<?> testClass) {
        Object obj = null;
        try {
            obj = testClass.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        methods = new ArrayList<Method>();
        for (Method each : testClass.getDeclaredMethods()) {
            if (each.getName() != "main") {
                methods.add(each);
                try {
                    each.invoke(obj);
                    succeded++;
                } catch (InvocationTargetException e) {
                    try {
                        throw e.getCause();
                    } catch (AssertionFailed a) {
                        failed++;
                        failedMessage += each.getName() + "\n";
                    } catch (Throwable throwable) {
                        throwable.printStackTrace();
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
