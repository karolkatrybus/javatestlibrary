package RingBufferTests.container;

import java.util.ArrayList;

public class RingBuffer {

    protected int capacity;
    protected int size = 0;
    protected ArrayList<Object> buffer;

    public RingBuffer(int capacity) {
        this.capacity = capacity;
        this.buffer = new ArrayList<>();
    }

    public boolean empty(){
        if (size() == 0)
            return true;
        else
            return false;
    }

    public int capacity(){
        return capacity;
    }

    public int size(){
        return size;
    }

    public boolean full(){
        if(size == capacity)
            return true;
        else
            return false;
    }

    public void push(Object val){
        if(full())
            pop();
        buffer.add(val);
        size++;
    }

    public Object pop(){
        if(!empty())
            size--;
        return buffer.remove(0);
    }

    public Object tail(){
        if(!empty())
            return buffer.get(0);
        return null;
    }

    public Object head(){
        if(!empty())
            return buffer.get(size-1);
        return null;
    }

    public Object at(int i){
        if(buffer.size() <= i)
            return null;
        return buffer.get(i);

    }


}
