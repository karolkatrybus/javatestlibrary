package RingBufferTests.tests;

import RingBufferTests.container.RingBuffer;
import javatestlibrary.TestCase;


public class RingBufferTests extends TestCase {
    public static void main(String[] args) {
        TestCase test = new RingBufferTests();
        test.run(RingBufferTests.class);
    }

    public void testCreate() {
        RingBuffer rbt = new RingBuffer(1);
        assertTrue(rbt.empty());
    }

    public void testShouldBeEmpty() {
        RingBuffer rbt = new RingBuffer(1);
        assertTrue(rbt.empty());
    }

    public void testCapacity() {
        RingBuffer rbt = new RingBuffer(3);
        assertEqual(3, rbt.capacity());
    }

    public void testSizeShouldBeZeroWhenEmpty() {
        RingBuffer rbt = new RingBuffer(3);
        assertTrue(rbt.empty());
        assertEqual(0, rbt.size());
    }

    public void testPushValueSizeShouldIncrease() {
        RingBuffer rbt = new RingBuffer(3);
        assertEqual(0, rbt.size());

        rbt.push(10);

        assertEqual(1, rbt.size());
    }

    public void testEmptyShouldBeFalseWhenFull() {
        RingBuffer rbt = new RingBuffer(1);

        assertTrue(rbt.empty());
        assertFalse(rbt.full());

        rbt.push("foo");

        assertFalse(rbt.empty());
        assertTrue(rbt.full());
    }

    public void testPushAndPopSingleValue() {
        RingBuffer rbt = new RingBuffer(1);

        rbt.push(10);

        assertEqual(10, rbt.pop());
    }

    public void testPopShouldReduceSize()
    {
        RingBuffer rbt = new RingBuffer(1);
        rbt.push(10);

        assertEqual(1, rbt.size());

        rbt.pop();

        assertEqual(0, rbt.size());
    }

    public void testBackShouldNotChangeBuffer() {
        RingBuffer rbt = new RingBuffer(1);
        rbt.push(10);

        assertEqual(1, rbt.size());

        assertEqual(10, rbt.tail());

        assertEqual(1, rbt.size());
    }


    public void testPushMultipleTimes()
    {
        RingBuffer rbt = new RingBuffer(1);

        rbt.push(10);
        assertEqual(1, rbt.size());
        assertEqual(10, rbt.tail());

        rbt.push(20);
        assertEqual(1, rbt.size());
        assertEqual(20, rbt.tail());

        rbt.push(30);

        assertEqual(1, rbt.size());
        assertEqual(30, rbt.tail());

        assertEqual(30, rbt.pop());
        assertEqual(0, rbt.size());

        //assertNull(rbt.tail());
    }

    public void testMultiItemBuffer()
    {
        RingBuffer rbt = new RingBuffer(3);

        assertTrue(rbt.empty());

        rbt.push(10);
        rbt.push(20);
        rbt.push(30);

        assertTrue(rbt.full());

        System.out.println(rbt.size());

        assertEqual(10, rbt.tail());
        assertEqual(30, rbt.head());

    }

    public void testPushAndPopAllValues()
    {
        RingBuffer rbt = new RingBuffer(9);

        rbt.push("Hello");
        rbt.push("!");
        rbt.push("How");
        rbt.push("are");
        rbt.push("You");
        rbt.push("doing");
        rbt.push("right");
        rbt.push("now");
        rbt.push("?");

        assertEqual("?", rbt.head());
        assertEqual("Hello", rbt.tail());

        assertEqual("Hello", rbt.pop());
        assertEqual("!", rbt.tail());

       assertEqual("!", rbt.pop());
        assertEqual("How", rbt.tail());

        assertEqual("How", rbt.pop());
        assertEqual("are", rbt.tail());

        assertEqual("are", rbt.pop());
        assertEqual("You", rbt.tail());

        assertEqual("You", rbt.pop());
        assertEqual("doing", rbt.tail());

        assertEqual("doing", rbt.pop());
        assertEqual("right", rbt.tail());

        assertEqual("right", rbt.pop());
        assertEqual("now", rbt.tail());

        assertEqual("now", rbt.pop());
        assertEqual("?", rbt.tail());

        assertEqual("?", rbt.pop());
        assertEqual(null, rbt.tail());
    }

    public void testAccessValuesByIndex()
    {
        RingBuffer rbt = new RingBuffer(9);

        rbt.push("Foo");
        rbt.push("Bar");
        rbt.push("Baz");

        assertEqual("Foo", rbt.at(0));
        assertEqual("Bar", rbt.at(1));
        assertEqual("Baz", rbt.at(2));
        assertEqual(null, rbt.at(3));
        assertEqual(null, rbt.at(4));
    }

    public void testIntValuesTest()
    {
        int n = 3;
        int k = 6;

        assertGreaterThan(n, k);
        assertGreaterOrEqual(n, k);
        assertLessThan(n, k);
        assertLessOrEqual(n, k);

    }

    public void testFloatValuesTest()
    {
        float a = (float) 1.1;
        float b = (float) 2.2;

        assertGreaterThan(a, b);
        assertLessThan(a, b);
        assertLessOrEqual(a, b);

    }

    public void testDoubleValuesTest()
    {
        double x = 1.1;
        double y = 2.2;

        assertGreaterThan(x, y);
        assertGreaterOrEqual(x, y);
        assertLessThan(x, y);
        assertLessOrEqual(x, y);

    }

    public void testIntDoubleValuesTest(){

        double d = 1.1;
        int i = 1;

        assertGreaterThan(i, d);
        assertGreaterThan(d, i);

        assertGreaterOrEqual(i, d);
        assertGreaterOrEqual(d, i);

    }

    public void testTwoStringsTest(){

        String s1 = "abc";
        String s2 = "abc";

        assertStringsEqual(s1, s2);
        assertStringsNotEqual(s1, s2 + "not");

    }

}
